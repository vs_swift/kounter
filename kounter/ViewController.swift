//
//  ViewController.swift
//  kounter
//
//  Created by Private on 1/27/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var Button: UIButton!
    
    var clickCounter: Int = 0
    var timer: Timer = Timer()
    
    @IBAction func resetButton(_ sender: Any) {
        clickCounter = 0
        Label.text = "\(clickCounter)"
    }
    
    @IBAction func ButtonDown(_ sender: Any) {
    }
    
    @IBAction func longPressHandler(_ sender: UILongPressGestureRecognizer) {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        Button.addGestureRecognizer(longPress)

        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.handleTap(_:)))
        tap.require(toFail: longPress)
        Button.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UIGestureRecognizer) {
        clickCounter+=1
        Label.text = "\(clickCounter)"
    }

    @objc func handleLongPress(_ sender: UIGestureRecognizer) {
        if sender.state == .began {
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(ViewController.handleTap(_:)), userInfo: nil, repeats: true)
    } else if sender.state == .ended || sender.state == .cancelled || (sender.state == .changed && !sender.view!.bounds.contains(sender.location(in: sender.view))) {
        timer.invalidate()
        }
    }
}

